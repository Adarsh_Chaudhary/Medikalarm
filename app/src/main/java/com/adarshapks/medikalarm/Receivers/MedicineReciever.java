package com.adarshapks.medikalarm.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.adarshapks.medikalarm.Activities.AlertMedicine;
import com.adarshapks.medikalarm.Helpers.DbHelper;
import com.adarshapks.medikalarm.Models.Medicine;

/**
 * Created by adarsh on 1/3/17.
 */

public class MedicineReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        DbHelper dbHelper = new DbHelper(context);
        Medicine med = dbHelper.getMedicine(intent.getIntExtra("Medicine ID", -1));
        Intent i = new Intent(context.getApplicationContext(), AlertMedicine.class);
        i.putExtra("RecievedMed", 1);
        i.putExtra("Medicine DetailID", med.getMedId());
        Log.d("IdatReciever", String.valueOf(med.getMedId()));

        i.setClassName("com.adarshapks.medikalarm", "com.adarshapks.medikalarm.Activities.AlertMedicine");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
