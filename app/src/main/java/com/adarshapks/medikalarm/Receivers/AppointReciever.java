package com.adarshapks.medikalarm.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.adarshapks.medikalarm.Activities.AlertAppointment;
import com.adarshapks.medikalarm.Helpers.DbHelper;
import com.adarshapks.medikalarm.Models.Appointment;

/**
 * Created by Adarsh on 23-02-2017.
 */

public class AppointReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        DbHelper dbHelper = new DbHelper(context);
        Appointment ap = dbHelper.getAppointment(intent.getIntExtra("Appointment ID", -1));
        Intent i = new Intent(context.getApplicationContext(), AlertAppointment.class);
        i.putExtra("RecievedAppoint", 1);
        i.putExtra("Appointment DetailID", ap.getAppointId());
        i.setClassName("com.adarshapks.medikalarm", "com.adarshapks.medikalarm.Activities.AlertAppointment");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
