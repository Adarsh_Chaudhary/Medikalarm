package com.adarshapks.medikalarm.Fragments;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adarshapks.medikalarm.Activities.AddMedActivity;
import com.adarshapks.medikalarm.Activities.AlertMedicine;
import com.adarshapks.medikalarm.Helpers.DbHelper;
import com.adarshapks.medikalarm.Helpers.ImageConvertor;
import com.adarshapks.medikalarm.Models.Medicine;
import com.adarshapks.medikalarm.R;
import com.adarshapks.medikalarm.Receivers.MedicineReciever;

import java.util.List;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by Adarsh on 22-02-2017.
 */

public class MedicineFragment extends Fragment {
    FloatingActionButton fab_add;
    public DbHelper dbHelper;
    public RecyclerView recyclerMed;
    public MedAdapter adapter;
    boolean isSet = false, allMed = false;
    List<Medicine> items;
    TextView tv_error;
    Context context = getActivity();

    public static MedicineFragment newInstance(boolean allMed) {
        MedicineFragment f = new MedicineFragment();
        Bundle b = new Bundle();
        b.putBoolean("FromAllActivity", allMed);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            allMed = getArguments().getBoolean("FromAllActivity", false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View med_view = inflater.inflate(R.layout.fragment_medicine, container, false);
        isSet = false;
        fab_add = (FloatingActionButton) med_view.findViewById(R.id.fab_add);
        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent launch_add = new Intent(getActivity(), AddMedActivity.class);
                launch_add.putExtra("Add", 0);
                startActivity(launch_add);
                getActivity().finish();
            }
        });
        context = getActivity();
        tv_error = (TextView) med_view.findViewById(R.id.tv_error);
        dbHelper = new DbHelper(getActivity());
        Log.d("Activity", "--" + getActivity());
        recyclerMed = (RecyclerView) med_view.findViewById(R.id.recycler);
        if (!allMed)
            items = dbHelper.getActiveMedicines();
        else
            items = dbHelper.getAllMedicines();
        recyclerMed.setItemAnimator(new DefaultItemAnimator());
        recyclerMed.setHasFixedSize(true);
        recyclerMed.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new MedAdapter();
        recyclerMed.setAdapter(adapter);
        return med_view;
    }

    public class MedAdapter extends RecyclerView.Adapter<MedAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.med_list, parent, false);
            ViewHolder viewHolder = new ViewHolder(convertView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final MedAdapter.ViewHolder holder, final int position) {
            final Medicine codeListBeans = items.get(position);
            holder.tv_medName.setText(codeListBeans.getMedName());
            if (getItemCount() < 0) {
                tv_error.setVisibility(View.VISIBLE);
                tv_error.setText(tv_error.getText().toString() + " Check in all Medicines");
            } else
                tv_error.setVisibility(View.GONE);
            if (codeListBeans.getSched()==1){
                holder.tv_medSched.setText("As Per Need");
            }
            if (codeListBeans.getMedRep() != 0) {
                if ((codeListBeans.getMedRep()) / (1000 * 60 * 60 * 24) == 1)
                    holder.tv_medSched.setText("Take Medicine Everyday");
                else
                    holder.tv_medSched.setText("Take Medicine every " + String.valueOf((codeListBeans.getMedRep()) / (1000 * 60 * 60 * 24)) + " days");
            }
            holder.iv_medImage.setImageBitmap((ImageConvertor.getPhoto(codeListBeans.getMedImg())));
            holder.meddListOpt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogUtil(codeListBeans, position);
                }
            });
            AlarmManager alarmManager;
            Intent intent = new Intent(getActivity(), MedicineReciever.class);
            intent.putExtra("Medicine ID", codeListBeans.getMedId());
            Log.d("IDatFragment", String.valueOf(codeListBeans.getMedId()));
            if ((getItemCount() != 0) && (codeListBeans.getCurrMilli() >= System.currentTimeMillis()) && isSet == false) {
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), codeListBeans.getMedId(), intent, 0);
                alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC_WAKEUP, codeListBeans.getCurrMilli(), pendingIntent);
                isSet = true;
                Log.d("pendingmedicine", " --" + codeListBeans.getCurrMilli());
            }
        }

        @Override
        public int getItemCount() {
            return items.size();

        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tv_medName, tv_medSched;
            ImageView iv_medImage;
            RelativeLayout meddListOpt;

            public ViewHolder(View itemView) {
                super(itemView);
                meddListOpt = (RelativeLayout) itemView.findViewById(R.id.medListOpt);
                iv_medImage = (ImageView) itemView.findViewById(R.id.iv_medImage);
                tv_medName = (TextView) itemView.findViewById(R.id.tv_medName);
                tv_medSched = (TextView) itemView.findViewById(R.id.tv_medSchedule);
            }
        }
    }

    private void dialogUtil(final Medicine m, final int position) {
        Button dialog_cancel, dialog_delete, dialog_update, dialog_view;
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.click_dialogbox);

        dialog_cancel = (Button) dialog.findViewById(R.id.dialog_cancel);
        dialog_delete = (Button) dialog.findViewById(R.id.dialog_delete);
        dialog_update = (Button) dialog.findViewById(R.id.dialog_update);
        dialog_view = (Button) dialog.findViewById(R.id.dialog_view);
        dialog.show();

        dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent med_Detail = new Intent(getActivity(), AlertMedicine.class);
                Log.d("ID", String.valueOf(m.getMedId()));
                med_Detail.putExtra("RecievedMed", 2);
                med_Detail.putExtra("Medicine DetailID", m.getMedId());
                startActivity(med_Detail);
            }
        });
        dialog_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent app_update = new Intent(getActivity(), AddMedActivity.class);
                app_update.putExtra("UpdateMed", 1);
                app_update.putExtra("Medicine DetailID", m.getMedId());
                startActivity(app_update);
                getActivity().finish();

            }
        });
        dialog_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dbHelper.deleteMedicine(m);
                items.remove(position);
                recyclerMed.removeViewAt(position);
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position, items.size());
            }
        });
    }
}