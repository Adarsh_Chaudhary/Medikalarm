package com.adarshapks.medikalarm.Fragments;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adarshapks.medikalarm.Activities.AddAppActivity;
import com.adarshapks.medikalarm.Activities.AlertAppointment;
import com.adarshapks.medikalarm.Receivers.AppointReciever;
import com.adarshapks.medikalarm.Helpers.DbHelper;
import com.adarshapks.medikalarm.Models.Appointment;
import com.adarshapks.medikalarm.R;

import java.util.List;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by Adarsh on 22-02-2017.
 */

public class AppointmentFragment extends Fragment {
    FloatingActionButton fab_add;
    public DbHelper dbHelper;
    RecyclerView recyclerMed;
    public AppAdapter adapter;
    List<Appointment> aItems;
    boolean isSet = false, allApp = false;
    TextView tv_error;

    public static AppointmentFragment newInstance(Boolean allApp) {
        AppointmentFragment f = new AppointmentFragment();
        Bundle b = new Bundle();
        b.putBoolean("FromAllActivity", allApp);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            allApp = getArguments().getBoolean("FromAllActivity", false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View app_view = inflater.inflate(R.layout.fragment_medicine, container, false);
        fab_add = (FloatingActionButton) app_view.findViewById(R.id.fab_add);
        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent launch_add = new Intent(getActivity(), AddAppActivity.class);
                launch_add.putExtra("Add", 0);
                getActivity().finish();
                startActivity(launch_add);
            }
        });
        dbHelper = new DbHelper(getActivity());
        if (allApp == false)
            aItems = dbHelper.getActiveAppointment();
        else
            aItems = dbHelper.getAllAppointment();

        tv_error = (TextView) app_view.findViewById(R.id.tv_error);
        recyclerMed = (RecyclerView) app_view.findViewById(R.id.recycler);
        recyclerMed.setItemAnimator(new DefaultItemAnimator());
        recyclerMed.setHasFixedSize(true);
        recyclerMed.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new AppAdapter();
        Log.d("count", String.valueOf(aItems.size()));
        recyclerMed.setAdapter(adapter);
        return app_view;
    }

    public class AppAdapter extends RecyclerView.Adapter<AppAdapter.ViewHolder> {
        public AppAdapter() {
        }

        @Override
        public AppAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_list, parent, false);
            AppAdapter.ViewHolder viewHolder = new AppAdapter.ViewHolder(convertView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final AppAdapter.ViewHolder holder, final int position) {

            final Appointment codeListBeans = aItems.get(position);
            if (aItems.size() <= 0) {
                tv_error.setText(tv_error.getText().toString() + "\nCheck in All Appointments Section for Inactive ones");
                tv_error.setVisibility(View.VISIBLE);
            } else
                tv_error.setVisibility(View.GONE);
            holder.tv_appName.setText("Appointment with " + codeListBeans.getAppointName()
                    + " at " + codeListBeans.getAppointTime() + " on " + codeListBeans.getAppointDate());
            Log.d("Details", codeListBeans.getAppointName() + " " + codeListBeans.getAppointDate() + " " + codeListBeans.getAppointTime());
            if (codeListBeans.getAppointAlarm() == 1) {
                holder.iv_appointList.setColorFilter(Color.argb(255, 16, 16, 207));
                holder.tv_appAlarm.setText(R.string.list_alarm_on);
            } else {
                holder.iv_appointList.setColorFilter(Color.argb(255, 123, 123, 134));
                holder.tv_appAlarm.setText(R.string.list_alarm_off);
            }

            AlarmManager alarmManager;
            Intent intent = new Intent(getActivity(), AppointReciever.class);
            intent.putExtra("Appointment ID", codeListBeans.getAppointId());
            Log.d("AppointAlarm", String.valueOf(position) + String.valueOf(codeListBeans.getAppointAlarm()));
            if ((getItemCount() != 0) && (codeListBeans.getAppointAlarm() == 1) && isSet == false) {
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), codeListBeans.getAppointId(), intent, 0);
                alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC_WAKEUP, codeListBeans.getAppointMillis(), pendingIntent);
                tv_error.setVisibility(View.GONE);
                isSet = true;
            }

            holder.appListOpt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogUtil(codeListBeans, position);
                }
            });
        }

        @Override
        public int getItemCount() {
            return aItems.size();

        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tv_appName, tv_appAlarm;
            RelativeLayout appListOpt;
            ImageView iv_appointList;

            public ViewHolder(View itemView) {
                super(itemView);
                iv_appointList = (ImageView) itemView.findViewById(R.id.iv_appointList);
                appListOpt = (RelativeLayout) itemView.findViewById(R.id.appListOpt);
                tv_appName = (TextView) itemView.findViewById(R.id.tv_appName);
                tv_appAlarm = (TextView) itemView.findViewById(R.id.tv_appAlarm);
            }
        }
    }

    private void dialogUtil(final Appointment a, final int position) {
        Button dialog_cancel, dialog_delete, dialog_update, dialog_view;
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.click_dialogbox);
        dialog_cancel = (Button) dialog.findViewById(R.id.dialog_cancel);
        dialog_delete = (Button) dialog.findViewById(R.id.dialog_delete);
        dialog_update = (Button) dialog.findViewById(R.id.dialog_update);
        dialog_view = (Button) dialog.findViewById(R.id.dialog_view);
        dialog.show();

        dialog_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent app_Detail = new Intent(getActivity(), AlertAppointment.class);
                Log.d("ID", String.valueOf(a.getAppointId()));
                app_Detail.putExtra("RecievedAppoint", 2);
                app_Detail.putExtra("Appointment DetailID", a.getAppointId());
                startActivity(app_Detail);

            }
        });
        dialog_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent app_update = new Intent(getActivity(), AddAppActivity.class);
                app_update.putExtra("UpdateApp", 1);
                app_update.putExtra("Appointment DetailID", a.getAppointId());
                startActivity(app_update);
            }
        });
        dialog_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dbHelper.deleteAppointment(a);
                aItems.remove(position);
                recyclerMed.removeViewAt(position);
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position, aItems.size());
            }
        });
        dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }
}
