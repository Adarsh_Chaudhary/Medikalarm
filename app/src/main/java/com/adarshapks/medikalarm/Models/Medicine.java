package com.adarshapks.medikalarm.Models;

/**
 * Created by Adarsh on 05-02-2017.
 */
public class Medicine {
    public Medicine(String name, byte[] img, int medType, String dosage, String instruction, int sched, String medStart,
                    String medEnd, int medFreq, long medRep, String medTimes, long startMilli, long endMilli, String allMilli) {
        this.dose = dosage;
        this.medName = name;
        this.inst = instruction;
        this.sched = sched;
        this.medImg = img;
        this.medType = medType;
        this.medStart = medStart;
        this.medEnd = medEnd;
        this.medTimes = medTimes;
        this.medRep = medRep;
        this.medFreq = medFreq;
        this.startMilli = startMilli;
        this.endMilli = endMilli;
        this.allMilli = allMilli;
    }


    public Medicine() {

    }

    public Medicine(int id,String name, byte[] img, int medType, String dosage, String instruction, int sched, String medStart,
                    String medEnd, int medFreq, long medRep, String medTimes, long currMilli, long startMilli, long endMilli,
                    String allMilli) {
        this.dose = dosage;
        this.medId=id;
        this.medName = name;
        this.inst = instruction;
        this.sched = sched;
        this.medImg = img;
        this.medType = medType;
        this.medStart = medStart;
        this.medEnd = medEnd;
        this.medTimes = medTimes;
        this.medRep = medRep;
        this.medFreq = medFreq;
        this.currMilli = currMilli;
        this.startMilli = startMilli;
        this.endMilli = endMilli;
        this.allMilli = allMilli;
    }


    public int getMedFreq() {
        return medFreq;
    }

    public void setMedFreq(int medFreq) {
        this.medFreq = medFreq;
    }

    public long getMedRep() {
        return medRep;
    }

    public void setMedRep(int medRep) {
        this.medRep = medRep;
    }

    public String getMedTimes() {
        return medTimes;
    }

    public void setMedTimes(String medTimes) {
        this.medTimes = medTimes;
    }

    public String getMedEnd() {
        return medEnd;
    }

    public void setMedEnd(String medEnd) {
        this.medEnd = medEnd;
    }

    public String getMedStart() {
        return medStart;
    }

    public void setMedStart(String medStart) {
        this.medStart = medStart;
    }

    public int getMedType() {
        return medType;
    }

    public void setMedType(int medType) {
        this.medType = medType;
    }

    public int getMedId() {
        return medId;
    }

    public void setMedId(int medId) {
        this.medId = medId;
    }

    public String getMedName() {
        return medName;
    }

    public void setMedName(String medName) {
        this.medName = medName;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getInst() {
        return inst;
    }

    public void setInst(String inst) {
        this.inst = inst;
    }

    public int getSched() {
        return sched;
    }

    public void setSched(int sched) {
        this.sched = sched;
    }

    public byte[] getMedImg() {
        return medImg;
    }

    public void setMedImg(byte[] medImg) {
        this.medImg = medImg;
    }

    public void setMedRep(long medRep) {
        this.medRep = medRep;
    }

    public long getStartMilli() {
        return startMilli;
    }

    public void setStartMilli(long startMilli) {
        this.startMilli = startMilli;
    }

    public long getEndMilli() {
        return endMilli;
    }

    public void setEndMilli(long endMilli) {
        this.endMilli = endMilli;
    }

    public long getCurrMilli() {
        return currMilli;
    }

    public void setCurrMilli(long currMilli) {
        this.currMilli = currMilli;
    }

    public String getAllMilli() {
        return allMilli;
    }

    public void setAllMilli(String allMilli) {
        this.allMilli = allMilli;
    }

    String medName, dose, inst, medStart, medEnd, medTimes, allMilli;
    byte[] medImg;
    int sched, medType, medId, medFreq;
    long medRep, startMilli, endMilli, currMilli;

}
