package com.adarshapks.medikalarm.Models;

/**
 * Created by Adarsh on 22-02-2017.
 */

public class Appointment {
    public Appointment(String appointName, String appointSubject, String appointVenue, String appointTime, String appointDate, Long appointMillis,
                       int appointAlarm) {
        this.appointName = appointName;
        this.appointAlarm = appointAlarm;
        this.appointSubject = appointSubject;
        this.appointDate = appointDate;
        this.appointTime = appointTime;
        this.appointVenue = appointVenue;
        this.appointMillis = appointMillis;
    }

    public Appointment(int appointId, String appointName, String appointSubject, String appointVenue, String appointTime, String appointDate, Long appointMillis,
                       int appointAlarm) {
        this.appointName = appointName;
        this.appointId = appointId;
        this.appointAlarm = appointAlarm;
        this.appointSubject = appointSubject;
        this.appointDate = appointDate;
        this.appointTime = appointTime;
        this.appointVenue = appointVenue;
        this.appointMillis = appointMillis;
    }


    public Appointment() {
    }

    public String getAppointName() {
        return appointName;
    }

    public void setAppointName(String appointName) {
        this.appointName = appointName;
    }

    public String getAppointVenue() {
        return appointVenue;
    }

    public void setAppointVenue(String appointVenue) {
        this.appointVenue = appointVenue;
    }

    public String getAppointTime() {
        return appointTime;
    }

    public void setAppointTime(String appointTime) {
        this.appointTime = appointTime;
    }

    public String getAppointDate() {
        return appointDate;
    }

    public void setAppointDate(String appointDate) {
        this.appointDate = appointDate;
    }

    public int getAppointId() {
        return appointId;
    }

    public void setAppointId(int appointId) {
        this.appointId = appointId;
    }

    public String getAppointSubject() {
        return appointSubject;
    }

    public void setAppointSubject(String appointSubject) {
        this.appointSubject = appointSubject;
    }

    public Long getAppointMillis() {
        return appointMillis;
    }

    public void setAppointMillis(Long appointMills) {
        this.appointMillis = appointMills;
    }

    public int getAppointAlarm() {
        return appointAlarm;
    }

    public void setAppointAlarm(int appointAlarm) {
        this.appointAlarm = appointAlarm;
    }

    String appointName, appointVenue, appointTime, appointDate, appointSubject;
    int appointId;
    int appointAlarm;
    Long appointMillis;

}
