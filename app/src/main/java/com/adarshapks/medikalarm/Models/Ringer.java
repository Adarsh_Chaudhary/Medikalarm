package com.adarshapks.medikalarm.Models;

/**
 * Created by adarsh on 14/3/17.
 */
public class Ringer {

    public Ringer(int ringId, String medRing, String appRing, int vibe) {
        this.ringId = ringId;
        this.ringMed = medRing;
        this.ringApp = appRing;
        this.vibe = vibe;
    }

    public Ringer() {

    }

    public int getRingId() {
        return ringId;
    }

    public void setRingId(int ringId) {
        this.ringId = ringId;
    }

    public int getVibe() {
        return vibe;
    }

    public void setVibe(int vibe) {
        this.vibe = vibe;
    }

    public String getRingMed() {
        return ringMed;
    }

    public void setRingMed(String ringMed) {
        this.ringMed = ringMed;
    }

    public String getRingApp() {
        return ringApp;
    }

    public void setRingApp(String ringApp) {
        this.ringApp = ringApp;
    }

    int ringId, vibe;
    String ringMed, ringApp;

}
