package com.adarshapks.medikalarm.Helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import com.adarshapks.medikalarm.Models.Appointment;
import com.adarshapks.medikalarm.Models.Medicine;
import com.adarshapks.medikalarm.Models.Ringer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Adarsh on 05-02-2017.
 */
public class DbHelper extends SQLiteOpenHelper {
    //changes made
    static int dbVersion = 54;
    static final String dbName = "medik";
    //For Medicine
    static final String TBL_MED = "medicDet", MED_ID = "medId", MED_NAME = "medName", MED_IMG = "medImg",
            MED_TYPE = "medType", MED_DOSE = "dose", MED_INST = "inst", MED_SCHED = "schedType",
            MED_START = "medStart", MED_END = "medEnd", MED_REPEAT = "medRepeat", MED_FREQ = "medFreq",
            MED_TIMES = "medTimes", MED_LATESTMILLI = "medLatAlarm", MED_STARTMILLI = "medStartMilli",
            MED_ENDMILLI = "medEndMilli", MED_TIMESMILLI = "medTimesMilli";
    //For Appointment
    static final String TBL_APPOINT = "appointDet", APP_ID = "appointId", APP_TIME = "appointTime",
            APP_DATE = "appointDate", APP_REASON = "appointReason", APP_VENUE = "appointVenue",
            APP_NAME = "appointName", APP_MILLIS = "appointMillis", ALARM_SET = "alarmSet";
    //For Ringtone
    static final String TBL_RINGER = "ringer", RINGID = "ringId", RING_MED = "ringMed", RING_APP = "ringApp",
            VIBE = "vibration";

    public DbHelper(Context context) {
        super(context, dbName, null, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Creating table for medicines
        String CREATE_MED_TABLE = "CREATE TABLE " + TBL_MED + "("
                + MED_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + MED_NAME + " TEXT, "
                + MED_IMG + " BLOB, "
                + MED_TYPE + " INTEGER, "
                + MED_DOSE + " TEXT, "
                + MED_INST + " TEXT, "
                + MED_SCHED + " INTEGER, "
                + MED_START + " TEXT, "
                + MED_END + " TEXT, "
                + MED_FREQ + " INTEGER, "
                + MED_REPEAT + " INTEGER DEFAULT 0, "
                + MED_TIMES + " TEXT, "
                + MED_LATESTMILLI + " INTEGER, "
                + MED_STARTMILLI + " INTEGER, "
                + MED_ENDMILLI + " INTEGER, "
                + MED_TIMESMILLI + " TEXT )";
        sqLiteDatabase.execSQL(CREATE_MED_TABLE);

        //Creating table for appointments
        String CREATE_APP_TABLE = "CREATE TABLE " + TBL_APPOINT + "("
                + APP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + APP_NAME + " TEXT, "
                + APP_REASON + " TEXT, "
                + APP_VENUE + " TEXT, "
                + APP_TIME + " TEXT, "
                + APP_DATE + " TEXT, "
                + APP_MILLIS + " INTEGER, "
                + ALARM_SET + " INTEGER )";
        sqLiteDatabase.execSQL(CREATE_APP_TABLE);

        //Creating table for Ringer
        String CREATE_RING_TABLE = "CREATE TABLE " + TBL_RINGER + "("
                + RINGID + " INTEGER PRIMARY KEY, "
                + RING_APP + " TEXT, "
                + RING_MED + " TEXT, "
                + VIBE + " INTEGER )";
        sqLiteDatabase.execSQL(CREATE_RING_TABLE);
        insertRinger(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //For table of medicine
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TBL_MED);
        //For table of appointments
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TBL_APPOINT);
        //For table of Ringtone
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TBL_RINGER);

        onCreate(sqLiteDatabase);
    }

    /*********************
     * For Medicine Table *
     *********************/

    public long addMedicine(Medicine medicine) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(MED_NAME, medicine.getMedName());
        values.put(MED_SCHED, medicine.getSched());
        values.put(MED_INST, medicine.getInst());
        values.put(MED_DOSE, medicine.getDose());
        values.put(MED_IMG, medicine.getMedImg());
        values.put(MED_TYPE, medicine.getMedType());
        values.put(MED_START, medicine.getMedStart());
        values.put(MED_END, medicine.getMedStart());
        values.put(MED_FREQ, medicine.getMedFreq());
        values.put(MED_REPEAT, medicine.getMedRep());
        values.put(MED_TIMES, medicine.getMedTimes());
        values.put(MED_STARTMILLI, medicine.getStartMilli());
        values.put(MED_ENDMILLI, medicine.getEndMilli());
        values.put(MED_TIMESMILLI, medicine.getAllMilli());

        String current[] = medicine.getAllMilli().split(",");
        values.put(MED_LATESTMILLI, current[0]);
        Cursor dbCursor = db.query(TBL_MED, null, null, null, null, null, null);
        String[] column = dbCursor.getColumnNames();
        Log.d("Column names", column[0] + column[1] + column[2] + column[3] + column[4] + column[5] + column[6] + column[7]
                + column[8] + column[9] + column[10] + column[11]);
        long id = db.insert(TBL_MED, null, values);

        Log.d("Insertion", String.valueOf(id));
        db.close(); // Closing database connection
        return (id);
    }

    public Medicine getMedicine(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TBL_MED, new String[]{MED_ID, MED_NAME, MED_IMG, MED_TYPE, MED_DOSE, MED_INST, MED_SCHED
                        , MED_START, MED_END, MED_FREQ, MED_REPEAT, MED_TIMES, MED_LATESTMILLI, MED_STARTMILLI, MED_ENDMILLI
                        , MED_TIMESMILLI}, MED_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Medicine medicine = new Medicine(cursor.getInt(0), cursor.getString(1), cursor.getBlob(2), cursor.getInt(3),
                cursor.getString(4), cursor.getString(5), cursor.getInt(6), cursor.getString(7),
                cursor.getString(8), cursor.getInt(9), cursor.getLong(10), cursor.getString(11)
                , cursor.getLong(12), cursor.getLong(13), cursor.getLong(14), cursor.getString(15));
        Log.d("DetailsSent", cursor.getString(1) + " " + cursor.getBlob(2) + " " + cursor.getInt(3) + " " +
                cursor.getString(4) + " " + cursor.getString(5) + " " + cursor.getInt(6) + " " + cursor.getString(7) +
                " " + cursor.getString(8) + " " + cursor.getInt(9) + " " + cursor.getLong(10) + " " + cursor.getString(11)
                + " " + cursor.getString(12) + " " + cursor.getLong(13) + " " + cursor.getLong(14) + " "
                + cursor.getString(15));
        db.close();
        return medicine;
    }

    public List<Medicine> getAllMedicines() {
        List<Medicine> medList = new ArrayList<Medicine>();
        String selectQuery = "SELECT  * FROM " + TBL_MED + " ORDER BY " + MED_LATESTMILLI + " ASC";       // Select All Query

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {                             // looping through all rows and adding to list
            do {
                Medicine med = new Medicine();
                med.setMedId(cursor.getInt(0));
                med.setMedName(cursor.getString(1));
                med.setMedImg((cursor.getBlob(2)));
                med.setMedType(cursor.getInt(3));
                med.setDose(cursor.getString(4));
                med.setInst(cursor.getString(5));
                med.setSched(cursor.getInt(6));
                med.setMedStart(cursor.getString(7));
                med.setMedEnd(cursor.getString(8));
                med.setMedFreq(cursor.getInt(9));
                med.setMedRep(cursor.getInt(10));
                med.setMedTimes(cursor.getString(11));
                med.setCurrMilli(cursor.getLong(12));
                med.setStartMilli(cursor.getLong(13));
                med.setEndMilli(cursor.getLong(14));
                med.setAllMilli(cursor.getString(15));

                Log.d("DetailsSent", cursor.getString(1) + " " + cursor.getBlob(2) + " " + cursor.getInt(3) + " " +
                        cursor.getString(4) + " " + cursor.getString(5) + " " + cursor.getInt(6) + " " + cursor.getString(7) +
                        " " + cursor.getString(8) + " " + cursor.getInt(9) + " " + cursor.getLong(10) + " " + cursor.getString(11)
                        + " " + cursor.getLong(12) + " " + cursor.getLong(13) + " " + cursor.getLong(14)
                        + " " + cursor.getString(15));
                medList.add(med);                               // Adding contact to list

            } while (cursor.moveToNext());
        }
        db.close();
        return medList;
    }

    public List<Medicine> getActiveMedicines() {
        List<Medicine> medList = new ArrayList<Medicine>();
        long currTime = System.currentTimeMillis();
        String selectQuery = "SELECT  * FROM " + TBL_MED + " WHERE " + MED_LATESTMILLI + ">" + currTime
                +" OR " + MED_SCHED +" = 1"+ " ORDER BY " + MED_LATESTMILLI + " ASC";       // Select All Query

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {                             // looping through all rows and adding to list
            do {
                Medicine med = new Medicine();
                med.setMedId(cursor.getInt(0));
                med.setMedName(cursor.getString(1));
                med.setMedImg((cursor.getBlob(2)));
                med.setMedType(cursor.getInt(3));
                med.setDose(cursor.getString(4));
                med.setInst(cursor.getString(5));
                med.setSched(cursor.getInt(6));
                med.setMedStart(cursor.getString(7));
                med.setMedEnd(cursor.getString(8));
                med.setMedFreq(cursor.getInt(9));
                med.setMedRep(cursor.getInt(10));
                med.setMedTimes(cursor.getString(11));
                med.setCurrMilli(cursor.getLong(12));
                med.setStartMilli(cursor.getLong(13));
                med.setEndMilli(cursor.getLong(14));
                med.setAllMilli(cursor.getString(15));

                medList.add(med);                               // Adding contact to list

            } while (cursor.moveToNext());
        }
        db.close();
        return medList;
    }

    public int updateMedicine(Medicine medicine, int medId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MED_NAME, medicine.getMedName());
        values.put(MED_SCHED, medicine.getSched());
        values.put(MED_INST, medicine.getInst());
        values.put(MED_DOSE, medicine.getDose());
        values.put(MED_IMG, medicine.getMedImg());
        values.put(MED_TYPE, medicine.getMedType());
        values.put(MED_START, medicine.getMedStart());
        values.put(MED_END, medicine.getMedStart());
        values.put(MED_FREQ, medicine.getMedFreq());
        values.put(MED_REPEAT, medicine.getMedRep());
        values.put(MED_TIMES, medicine.getMedTimes());
        values.put(MED_ENDMILLI, medicine.getEndMilli());
        values.put(MED_LATESTMILLI, medicine.getCurrMilli());
        values.put(MED_STARTMILLI, medicine.getStartMilli());
        values.put(MED_TIMESMILLI, medicine.getAllMilli());
        Cursor dbCursor = db.query(TBL_MED, null, null, null, null, null, null);
        int x = db.update(TBL_MED, values, MED_ID + " = ?",
                new String[]{String.valueOf(medId)});
        db.close();
        return x;
    }

    public void deleteMedicine(Medicine medicine) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TBL_MED, MED_ID + " = ?",
                new String[]{String.valueOf(medicine.getMedId())});
        db.close();
    }

    public int updateAlarmMedicine(int id, String newMill, int position) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MED_TIMESMILLI, newMill);
        String current[] = newMill.split(",");
        values.put(MED_LATESTMILLI, current[position]);
        int i = db.update(TBL_MED, values, MED_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
        return 0;
    }

    public Medicine nextAlarmMed() {
        SQLiteDatabase db = this.getWritableDatabase();
        long thisTime = System.currentTimeMillis();
        String query = "SELECT * FROM " + TBL_MED + " WHERE " + MED_LATESTMILLI + " > " + thisTime +
                " ORDER BY " + MED_LATESTMILLI + " ASC ";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            Medicine med = new Medicine();
            med.setMedId(cursor.getInt(0));
            med.setMedName(cursor.getString(1));
            med.setMedImg((cursor.getBlob(2)));
            med.setMedType(cursor.getInt(3));
            med.setDose(cursor.getString(4));
            med.setInst(cursor.getString(5));
            med.setSched(cursor.getInt(6));
            med.setMedStart(cursor.getString(7));
            med.setMedEnd(cursor.getString(8));
            med.setMedFreq(cursor.getInt(9));
            med.setMedRep(cursor.getInt(10));
            med.setMedTimes(cursor.getString(11));
            med.setCurrMilli(cursor.getLong(12));
            med.setStartMilli(cursor.getLong(13));
            med.setEndMilli(cursor.getLong(14));
            med.setAllMilli(cursor.getString(15));
            return med;
        }
        db.close();
        return null;
    }

    /***********************
     * For Appointment Table
     ***********************/

    public long addAppointment(Appointment appointment) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(APP_NAME, appointment.getAppointName());
        values.put(APP_REASON, appointment.getAppointSubject());
        values.put(APP_VENUE, appointment.getAppointVenue());
        values.put(APP_TIME, appointment.getAppointTime());
        values.put(APP_DATE, appointment.getAppointDate());
        values.put(APP_MILLIS, appointment.getAppointMillis());
        values.put(ALARM_SET, appointment.getAppointAlarm());
        Cursor dbCursor = db.query(TBL_APPOINT, null, null, null, null, null, null);
        long id = db.insert(TBL_APPOINT, null, values);
        db.close(); // Closing database connection
        return id;
    }

    public Appointment getAppointment(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TBL_APPOINT, new String[]{APP_ID, APP_NAME, APP_REASON, APP_VENUE, APP_TIME, APP_DATE,
                APP_MILLIS, ALARM_SET}, APP_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Appointment appointment = new Appointment(cursor.getInt(0), cursor.getString(1),
                cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getLong(6),
                cursor.getInt(7));
        Log.d("entry", (appointment.getAppointDate() + appointment.getAppointTime() + appointment.getAppointVenue() + appointment.getAppointSubject()
                + appointment.getAppointName()));
        db.close();
        return appointment;
    }

    public Appointment nextAlarmApp() {
        String query = "SELECT * FROM " + TBL_APPOINT + " WHERE " + ALARM_SET + " = 1 ORDER BY " + APP_MILLIS + " ASC ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            Appointment appointment = new Appointment();
            appointment.setAppointId(Integer.parseInt(cursor.getString(0)));
            appointment.setAppointName(cursor.getString(1));
            appointment.setAppointSubject(cursor.getString(2));
            appointment.setAppointVenue((cursor.getString(3)));
            appointment.setAppointTime(cursor.getString(4));
            appointment.setAppointDate(cursor.getString(5));
            appointment.setAppointMillis(cursor.getLong(6));
            appointment.setAppointAlarm(cursor.getInt(7));
            db.close();
            return appointment;
        }
        db.close();
        return null;
    }

    public List<Appointment> getAllAppointment() {
        List<Appointment> appointList = new ArrayList<Appointment>();
        String selectQuery = "SELECT  * FROM " + TBL_APPOINT + " ORDER BY " + APP_MILLIS + " ASC";       // Select All Query

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {                             // looping through all rows and adding to list
            do {
                Appointment appointment = new Appointment();
                appointment.setAppointId(Integer.parseInt(cursor.getString(0)));
                appointment.setAppointName(cursor.getString(1));
                appointment.setAppointSubject(cursor.getString(2));
                appointment.setAppointVenue((cursor.getString(3)));
                appointment.setAppointTime(cursor.getString(4));
                appointment.setAppointDate(cursor.getString(5));
                appointment.setAppointMillis(cursor.getLong(6));
                appointment.setAppointAlarm(cursor.getInt(7));
                Log.d("table entry", cursor.getString(1) + cursor.getString(2) + cursor.getString(3) + cursor.getString(4) +
                        cursor.getString(5) + cursor.getLong(6) + "+" + cursor.getInt(7));
                appointList.add(appointment);                               // Adding contact to list

            } while (cursor.moveToNext());
        }
        db.close();
        return appointList;
    }

    public List<Appointment> getActiveAppointment() {
        List<Appointment> appointList = new ArrayList<Appointment>();
        String selectQuery = "SELECT  * FROM " + TBL_APPOINT + " WHERE " + ALARM_SET + "= 1 "
                + " ORDER BY " + APP_MILLIS + " ASC";       // Select All Query

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {                             // looping through all rows and adding to list
            do {
                Appointment appointment = new Appointment();
                appointment.setAppointId(Integer.parseInt(cursor.getString(0)));
                appointment.setAppointName(cursor.getString(1));
                appointment.setAppointSubject(cursor.getString(2));
                appointment.setAppointVenue((cursor.getString(3)));
                appointment.setAppointTime(cursor.getString(4));
                appointment.setAppointDate(cursor.getString(5));
                appointment.setAppointMillis(cursor.getLong(6));
                appointment.setAppointAlarm(cursor.getInt(7));
                Log.d("table entry", cursor.getString(1) + cursor.getString(2) + cursor.getString(3) + cursor.getString(4) +
                        cursor.getString(5) + cursor.getLong(6) + "+" + cursor.getInt(7));
                appointList.add(appointment);                               // Adding contact to list

            } while (cursor.moveToNext());
        }
        db.close();
        return appointList;
    }

    public int updateAlarmAppointment(Appointment appointment, int Alarm) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(APP_NAME, appointment.getAppointName());
        values.put(APP_REASON, appointment.getAppointSubject());
        values.put(APP_VENUE, appointment.getAppointVenue());
        values.put(APP_TIME, appointment.getAppointTime());
        values.put(APP_DATE, appointment.getAppointDate());
        values.put(APP_MILLIS, appointment.getAppointMillis());
        values.put(ALARM_SET, Alarm);
        int x = db.update(TBL_APPOINT, values, APP_ID + " = ?",
                new String[]{String.valueOf(appointment.getAppointId())});
        db.close();
        return x;
    }

    public void deleteAppointment(Appointment appointment) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TBL_APPOINT, APP_ID + " = ?", new String[]{String.valueOf(appointment.getAppointId())});
        db.close();
    }

    public int updateAppointment(Appointment appointment, int appointId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(APP_NAME, appointment.getAppointName());
        values.put(APP_REASON, appointment.getAppointSubject());
        values.put(APP_VENUE, appointment.getAppointVenue());
        values.put(APP_TIME, appointment.getAppointTime());
        values.put(APP_DATE, appointment.getAppointDate());
        values.put(APP_MILLIS, appointment.getAppointMillis());
        values.put(ALARM_SET, appointment.getAppointAlarm());
        int x = db.update(TBL_APPOINT, values, APP_ID + " = ?",
                new String[]{String.valueOf(appointId)});
        db.close();
        return x;
    }

    /***********************
     * For Ringtone Table
     ***********************/

    public void insertRinger(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        values.put(RINGID, 1);
        values.put(RING_MED, String.valueOf(notification));
        values.put(RING_APP, String.valueOf(notification));
        values.put(VIBE, 1);
        Log.d("Ringtone", String.valueOf(notification));
        Cursor dbCursor = db.query(TBL_RINGER, null, null, null, null, null, null);
        long id = db.insert(TBL_RINGER, null, values);

    }

    public int updateRinger(String ringMed, String who) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (who.equals("Medicine")) {
            values.put(RING_MED, ringMed);
        } else if (who.equals("Appoint")) {
            values.put(RING_APP, ringMed);
        } else if (who.equals("Vibration")) {
            Log.d("Vibrate", (ringMed));
            if (ringMed.equals("false"))
                values.put(VIBE, 0);
            else
                values.put(VIBE, 1);
        }
        int x = db.update(TBL_RINGER, values, RINGID + " = ?",
                new String[]{String.valueOf(1)});
        db.close();
        return x;

    }

    public Ringer getRinger() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TBL_RINGER, new String[]{RINGID, RING_MED, RING_APP, VIBE}
                , RINGID + "=?", new String[]{String.valueOf(1)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Ringer ring = new Ringer(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3));
        Log.d("Ring entry", (ring.getRingId() + ring.getRingMed() + ring.getRingApp() + ring.getVibe()));
        db.close();
        return ring;
    }
}
