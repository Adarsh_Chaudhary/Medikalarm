package com.adarshapks.medikalarm.Activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.adarshapks.medikalarm.Helpers.DbHelper;
import com.adarshapks.medikalarm.Helpers.ImageConvertor;
import com.adarshapks.medikalarm.Models.Medicine;
import com.adarshapks.medikalarm.Models.Ringer;
import com.adarshapks.medikalarm.R;
import com.adarshapks.medikalarm.Receivers.MedicineReciever;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by adarsh on 5/3/17.
 */

public class AlertMedicine extends AppCompatActivity {
    EditText et_NameAlertMed, et_InstAlertMed;
    ImageView iv_ImageAlertMed;
    Button btn_medStop;
    DbHelper db;
    Ringtone notification;
    PendingIntent sender;
    AlarmManager alarmManager;
    MediaPlayer mp = new MediaPlayer();
    Medicine med = new Medicine();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alert_medicine);
        et_NameAlertMed = (EditText) findViewById(R.id.et_NameAlertMed);
        et_InstAlertMed = (EditText) findViewById(R.id.et_InstAlertMed);
        iv_ImageAlertMed = (ImageView) findViewById(R.id.iv_ImageAlertMed);
        btn_medStop = (Button) findViewById(R.id.btn_MedStop);
        db = new DbHelper(this);

        Ringer ring = db.getRinger();
        notification = RingtoneManager.getRingtone(getApplicationContext(), Uri.parse(ring.getRingMed()));
        mp = MediaPlayer.create(getApplicationContext(), Uri.parse(ring.getRingMed()));
        String quant="";
        if ((getIntent().getIntExtra("RecievedMed", 0)) != 0) {

            med = db.getMedicine(getIntent().getIntExtra("Medicine DetailID", -1));


            switch(med.getMedType()){
                case 1: quant="tablets";
                    break;
                case 2: quant="ml";
                    break;
                case 3: quant="spoonful";
                    break;
                case 4: quant="drop";
                    break;
            }
            Date date = new Date(med.getCurrMilli());
            DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
            DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(getApplicationContext());

            String timeDate= dateFormat.format(date) + " " + timeFormat.format(date);
            et_NameAlertMed.setText("Time to take "+med.getMedName()+". Take "+med.getDose()+" "+ quant+" as per the following instructions");
            et_InstAlertMed.setText(med.getInst()+" is "+timeDate);
            iv_ImageAlertMed.setImageBitmap(ImageConvertor.getPhoto(med.getMedImg()));
        }
        et_NameAlertMed.setClickable(false);
        et_NameAlertMed.setFocusable(false);
        et_InstAlertMed.setClickable(false);
        et_InstAlertMed.setFocusable(false);

        if ((getIntent().getIntExtra("RecievedMed", 0) == 1)) {
            Intent intent = new Intent(this, MedicineReciever.class);
            et_NameAlertMed.setText("Time to take "+med.getMedName()+". Take "+med.getDose()+" "+ quant+" as per the following instructions");
            et_InstAlertMed.setVisibility(View.VISIBLE);
            et_NameAlertMed.setVisibility(View.VISIBLE);
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            sender = PendingIntent.getBroadcast(this, 0, intent, 0);
            alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            if (ring.getVibe() == 1) {
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(1000);
            }
            btn_medStop.setVisibility(View.VISIBLE);
            String allMill[] = med.getAllMilli().split(",");
            int i, position = 0;
            String newMill = "";
            for (i = 0; i < allMill.length; i++) {
                if (med.getCurrMilli() == Long.parseLong(allMill[i])) {
                    position = i;
                    newMill = newMill.concat(String.valueOf(Long.parseLong(allMill[i]) + med.getMedRep()) + ",");
                } else
                    newMill = newMill.concat(allMill[i] + ",");
            }
            if (position == med.getMedFreq() - 1)
                position = 0;
            else
                position++;
            db.updateAlarmMedicine(med.getMedId(), newMill, position);
            Medicine next = db.nextAlarmMed();
            if (next != null) {
                intent.putExtra("Medicine ID", next.getMedId());
                sender = PendingIntent.getBroadcast(this, next.getMedId(), intent, 0);
                alarmManager.set(AlarmManager.RTC_WAKEUP, next.getCurrMilli(), sender);
            }
            mp.start();
            mp.setLooping(true);
        } else if (getIntent().getIntExtra("RecievedMed", 0) == 2) {

            et_InstAlertMed.setVisibility(View.VISIBLE);
            et_NameAlertMed.setVisibility(View.VISIBLE);
            btn_medStop.setVisibility(View.GONE);
        }

        btn_medStop.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                mp.stop();
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        mp.stop();
        finish();
    }

}
