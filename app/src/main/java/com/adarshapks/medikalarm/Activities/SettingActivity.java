package com.adarshapks.medikalarm.Activities;

import android.app.Activity;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import com.adarshapks.medikalarm.Helpers.DbHelper;
import com.adarshapks.medikalarm.Models.Ringer;
import com.adarshapks.medikalarm.R;

/**
 * Created by adarsh on 9/3/17.
 */
public class SettingActivity extends AppCompatActivity {
    Button btn_setMedRing, btn_setAppRing;
    TextView tv_medRing, tv_appRing;
    Switch btn_vibe;
    DbHelper db = new DbHelper(this);
    String medRingtone = "", appRingtone = "";
    int medRing = 0, appRing = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Settings");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left);

        btn_setAppRing = (Button) findViewById(R.id.btn_setAppRing);
        btn_setMedRing = (Button) findViewById(R.id.btn_setMedRing);
        btn_vibe = (Switch) findViewById(R.id.btn_vibration);
        tv_appRing = (TextView) findViewById(R.id.tv_appRing);
        tv_medRing = (TextView) findViewById(R.id.tv_medRing);

        final Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
        Ringer ringer = db.getRinger();
        Ringtone medRing = RingtoneManager.getRingtone(this, Uri.parse(ringer.getRingMed()));
        Ringtone appRing = RingtoneManager.getRingtone(this, Uri.parse(ringer.getRingApp()));
        medRingtone = medRingtone.concat(ringer.getRingMed());
        appRingtone = appRingtone.concat(ringer.getRingApp());

        Log.d("title", medRing.getTitle(this) + " " + appRing.getTitle(this));
        tv_medRing.setText(medRing.getTitle(this));
        tv_appRing.setText(appRing.getTitle(this));

        btn_setAppRing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(intent, 1001);
            }
        });
        btn_setMedRing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(intent, 1002);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add_save) {
            if (medRing == 1) {
                db.updateRinger(medRingtone, "Medicine");
            }
            if (appRing == 1) {
                db.updateRinger(appRingtone, "Appoint");
            }
            Log.d("Vibration", String.valueOf(btn_vibe.isChecked()));
            db.updateRinger(String.valueOf(btn_vibe.isChecked()), "Vibration");
            Intent launch_main = new Intent(SettingActivity.this, HomeActivity.class);
            startActivity(launch_main);
            finish();
            return true;
        } else if (id == R.id.add_cancel) {
            Intent launch_main = new Intent(SettingActivity.this, HomeActivity.class);
            startActivity(launch_main);
            finish();
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1002) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                if (uri != null) {
                    Log.d("Before", db.getRinger().getRingMed() + " " + db.getRinger().getRingApp());
                    medRingtone = uri.toString();
                    medRing = 1;
                    Ringtone ring = RingtoneManager.getRingtone(this, uri);
                    Log.d("Setting",ring.getTitle(this));

                    tv_medRing.setText(ring.getTitle(this));
                }
            }
        } else if (requestCode == 1001) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                if (uri != null) {
                    appRing = 1;
                    appRingtone = uri.toString();
                    Ringtone ring = RingtoneManager.getRingtone(this, uri);
                    tv_appRing.setText(ring.getTitle(this));

                }
            }
        }
    }
}
