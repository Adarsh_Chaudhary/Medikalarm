package com.adarshapks.medikalarm.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.adarshapks.medikalarm.Fragments.SlidingTabLayout;
import com.adarshapks.medikalarm.Fragments.ViewPagerAdapter;
import com.adarshapks.medikalarm.R;


public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    ViewPager view_pager;
    ViewPagerAdapter viewAdapter;
    SlidingTabLayout tabs;
    CharSequence titles[] = {"Medicine", "Appointment"};
    int noTabs = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        view_pager = (ViewPager) findViewById(R.id.pager);
        viewAdapter = new ViewPagerAdapter(getSupportFragmentManager(), titles, noTabs);
        view_pager.setAdapter(viewAdapter);
        if (getIntent().getIntExtra("Back to Home", 0) == 2) {
            view_pager.setCurrentItem(1);
        }
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setViewPager(view_pager);
        tabs.setDistributeEvenly(false);
        //tabs.setDistributeEvenly(false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_setting) {
            Intent launch_set = new Intent(HomeActivity.this, SettingActivity.class);
            startActivity(launch_set);
        } else if (id == R.id.nav_allMed) {
            Intent launch_allMed = new Intent(HomeActivity.this, AllActivity.class);
            launch_allMed.putExtra("WhichOne", "med");
            launch_allMed.putExtra("FromHomeActivity",1);
            startActivity(launch_allMed);

        } else if (id == R.id.nav_allApp) {
            Intent launch_allApp = new Intent(HomeActivity.this, AllActivity.class);
            launch_allApp.putExtra("FromHomeActivity",1);
            launch_allApp.putExtra("WhichOne", "app");
            startActivity(launch_allApp);

        } else if (id == R.id.nav_share) {
            Intent launch_share = new Intent();
            launch_share.setAction(Intent.ACTION_SEND);
            launch_share.putExtra(Intent.EXTRA_TEXT, "Open " + getString(R.string.app_name) + " app so that you wont ever forget the medication " + getPackageName());
            launch_share.setType("text/plain");
            startActivity(Intent.createChooser(launch_share, "Choose an option..."));
        } else if (id == R.id.nav_feed) {
            Intent launch_feed = new Intent(Intent.ACTION_SEND);
            launch_feed.putExtra(Intent.EXTRA_EMAIL, new String[]{"adarshchaudhary415@gmail.com"});
            launch_feed.putExtra(Intent.EXTRA_SUBJECT, "Medikalarm feedback");
            launch_feed.putExtra(Intent.EXTRA_TEXT, "");
            launch_feed.setType("message/rfc822");
            startActivity(Intent.createChooser(launch_feed, "Select an option"));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }
}
