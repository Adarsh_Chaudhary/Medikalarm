package com.adarshapks.medikalarm.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.adarshapks.medikalarm.Fragments.AppointmentFragment;
import com.adarshapks.medikalarm.Fragments.MedicineFragment;
import com.adarshapks.medikalarm.Models.Appointment;
import com.adarshapks.medikalarm.R;

/**
 * Created by adarsh on 16/3/17.
 */
public class AllActivity extends AppCompatActivity {
    FrameLayout fmLayout;
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Complete List");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left);

        fmLayout = (FrameLayout) findViewById(R.id.fragmentAll);
        if (this.getIntent().getIntExtra("FromHomeActivity", 0) == 1) {
            Log.d("AllActvity", getIntent().getIntExtra("FromHomeActivity", 0) + getIntent().getStringExtra("WhichOne"));

            if (getIntent().getStringExtra("WhichOne").equals("med")) {
                MedicineFragment medicineFragment = MedicineFragment.newInstance(true);
                ft.replace(R.id.fragmentAll, medicineFragment, "MedicineFragment");
                ft.commit();
            } else if (getIntent().getStringExtra("WhichOne").equals("app")) {
                AppointmentFragment appointmentFragment = AppointmentFragment.newInstance(true);
                ft.replace(R.id.fragmentAll, appointmentFragment, "AppointmentFragment");
                ft.commit();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
