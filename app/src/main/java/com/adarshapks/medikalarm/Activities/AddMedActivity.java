package com.adarshapks.medikalarm.Activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.adarshapks.medikalarm.Helpers.DbHelper;
import com.adarshapks.medikalarm.Helpers.ImageConvertor;
import com.adarshapks.medikalarm.Models.Medicine;
import com.adarshapks.medikalarm.R;

import java.util.Calendar;

/**
 * Created by Adarsh on 24-01-2017.
 */
public class AddMedActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    EditText et_medName, et_dose, et_inst;
    Spinner spin_type, spin_schedType, spin_schedfreq, spin_schedRep;
    FrameLayout fm_layout;
    Button btn_startD, btn_endD, btn_timeDays;
    Button[] btn_sched = new Button[13];
    TextView tv_expandDetails, tv_expandSchedule;
    LinearLayout ll_freq, ll_days, ll_date, ll_schedule;
    RelativeLayout rl_add;
   /* CheckBox cb_sun, cb_mon, cb_tue, cb_wed, cb_thu, cb_fri, cb_sat;
*/
    final Calendar cal = Calendar.getInstance();
    private int mYear, mMonth, mDay, mHour, mMinute;
    DbHelper dbHelper;
    Button btn_upImage;
    long startMilli = 0, endMilli = 0, currMilli = 0;
    Medicine med = new Medicine();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addmed);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left);

        tv_expandDetails = (TextView) findViewById(R.id.tv_expandDetails);
        tv_expandSchedule = (TextView) findViewById(R.id.tv_expandSchedule);
        et_medName = (EditText) findViewById(R.id.et_medName);
        et_dose = (EditText) findViewById(R.id.et_dose);
        et_inst = (EditText) findViewById(R.id.et_inst);
        fm_layout = (FrameLayout) findViewById(R.id.fmlayout);
        btn_upImage = (Button) findViewById(R.id.btn_upImage);
        btn_endD = (Button) findViewById(R.id.btn_endD);
        btn_startD = (Button) findViewById(R.id.btn_startD);
        btn_sched[0] = (Button) findViewById(R.id.btn_time1);
        btn_sched[1] = (Button) findViewById(R.id.btn_time2);
        btn_sched[2] = (Button) findViewById(R.id.btn_time3);
        btn_sched[3] = (Button) findViewById(R.id.btn_time4);
        btn_sched[4] = (Button) findViewById(R.id.btn_time5);
        btn_sched[5] = (Button) findViewById(R.id.btn_time6);
        btn_sched[6] = (Button) findViewById(R.id.btn_time7);
        btn_sched[7] = (Button) findViewById(R.id.btn_time8);
        btn_sched[8] = (Button) findViewById(R.id.btn_time9);
        btn_sched[9] = (Button) findViewById(R.id.btn_time10);
        btn_sched[10] = (Button) findViewById(R.id.btn_time11);
        btn_sched[11] = (Button) findViewById(R.id.btn_time12);
        /*btn_timeDays = (Button) findViewById(R.id.btn_timeDays);
        */spin_type = (Spinner) findViewById(R.id.spin_type);
        spin_schedfreq = (Spinner) findViewById(R.id.spin_schedFreq);
        spin_schedType = (Spinner) findViewById(R.id.spin_schedType);
        spin_schedRep = (Spinner) findViewById(R.id.spin_schedRep);
        /*cb_sun = (CheckBox) findViewById(R.id.cb_sun);
        cb_mon = (CheckBox) findViewById(R.id.cb_mon);
        cb_tue = (CheckBox) findViewById(R.id.cb_tue);
        cb_wed = (CheckBox) findViewById(R.id.cb_wed);
        cb_thu = (CheckBox) findViewById(R.id.cb_thu);
        cb_fri = (CheckBox) findViewById(R.id.cb_fri);
        cb_sat = (CheckBox) findViewById(R.id.cb_sat);
        */ll_schedule = (LinearLayout) findViewById(R.id.ll_schedule);
        ll_freq = (LinearLayout) findViewById(R.id.ll_freq);
        ll_date = (LinearLayout) findViewById(R.id.ll_date);
/*
        ll_days = (LinearLayout) findViewById(R.id.ll_days);
*/
        rl_add = (RelativeLayout) findViewById(R.id.rl_add);

        tv_expandDetails.setOnClickListener(this);
        tv_expandSchedule.setOnClickListener(this);
        btn_upImage.setOnClickListener(this);
        btn_endD.setOnClickListener(this);
        btn_startD.setOnClickListener(this);
        btn_sched[0].setOnClickListener(this);
        btn_sched[1].setOnClickListener(this);
        btn_sched[2].setOnClickListener(this);
        btn_sched[3].setOnClickListener(this);
        btn_sched[4].setOnClickListener(this);
        btn_sched[5].setOnClickListener(this);
        btn_sched[6].setOnClickListener(this);
        btn_sched[7].setOnClickListener(this);
        btn_sched[8].setOnClickListener(this);
        btn_sched[9].setOnClickListener(this);
        btn_sched[10].setOnClickListener(this);
        btn_sched[11].setOnClickListener(this);
/*
        btn_timeDays.setOnClickListener(this);
*/
        spin_schedRep.setOnItemSelectedListener(this);
        spin_schedType.setOnItemSelectedListener(this);
        spin_schedfreq.setOnItemSelectedListener(this);
        spin_type.setOnItemSelectedListener(this);
        dbHelper = new DbHelper(this);

        btn_startD.setText(cal.get(Calendar.DAY_OF_MONTH) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR));

        if ((getIntent().getIntExtra("UpdateMed", 0)) == 1) {
            Log.d("ID RECIEVED", String.valueOf(getIntent().getIntExtra("Medicine DetailID", -1)));
            med = dbHelper.getMedicine(getIntent().getIntExtra("Medicine DetailID", -1));
            et_medName.setText(med.getMedName());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                fm_layout.setBackground(new BitmapDrawable(ImageConvertor.getPhoto(med.getMedImg())));
            }
            spin_type.setSelection(med.getMedType());
            et_dose.setText(med.getDose());
            et_inst.setText(med.getInst());
            spin_schedType.setSelection(med.getSched());
            String time[] = med.getMedTimes().split(",");
            spin_schedfreq.setSelection(med.getMedFreq());
            rl_add.setVisibility(View.GONE);
            ll_schedule.setVisibility(View.GONE);
            for (int i = 0; i < med.getMedFreq(); i++) {
                btn_sched[i].setText(time[i]);
                btn_sched[i].setVisibility(View.VISIBLE);
            }
            if ((med.getMedRep()) / (1000 * 60 * 60 * 24) < 8) {
                spin_schedRep.setSelection((int) (med.getMedRep() / (1000 * 60 * 60 * 24)));
            } else if ((med.getMedRep()) / (1000 * 60 * 60 * 24) == 14) {
                spin_schedRep.setSelection(8);
            } else if ((med.getMedRep()) / (1000 * 60 * 60 * 24) == 21) {
                spin_schedRep.setSelection(9);
            } else if ((med.getMedRep()) / (1000 * 60 * 60 * 24) == 28) {
                spin_schedRep.setSelection(10);
            } else if ((med.getMedRep()) / (1000 * 60 * 60 * 24) == 30) {
                spin_schedRep.setSelection(11);
            } else if ((med.getMedRep()) / (1000 * 60 * 60 * 24) == 60) {
                spin_schedRep.setSelection(12);
            }
        }
    }

    private void displayDate(final Button b) {
        mYear = cal.get(Calendar.YEAR);
        mMonth = cal.get(Calendar.MONTH);
        mDay = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        if ((year >= cal.get(Calendar.YEAR)) && (monthOfYear >= cal.get(Calendar.MONTH)) && (dayOfMonth >= cal.get(Calendar.DAY_OF_MONTH))) {
                            b.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void displayTime(final Button btn) {
        // Get Current Time
        mHour = cal.get(Calendar.HOUR_OF_DAY);
        mMinute = cal.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        btn.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        DbHelper db = new DbHelper(this);
        BitmapDrawable bitmapDrawable;
        Bitmap bitmap = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            bitmapDrawable = (BitmapDrawable) fm_layout.getBackground();
            bitmap = bitmapDrawable.getBitmap();
        }

        Medicine m = new Medicine();

        if (id == R.id.add_save) {
            String times = "";
            String endDate[];
            long startMilli = 0, endMilli = 0;
            if (btn_endD.getText().toString().equalsIgnoreCase("forever(click to set date)")) {
                endDate = breakDate("31-12-2033");
            } else {
                endDate = breakDate(btn_endD.getText().toString());
            }
            String startDate[] = breakDate(btn_startD.getText().toString());

            String allMillis = "";
            int i = 0;
            if (validate(rl_add) == 0) {
                for (; (i < 12) && !(btn_sched[i].getText().equals("__:__")); i++) {
                    times = times.concat(btn_sched[i].getText() + ",");
                    String timer[] = breakTime(btn_sched[i].getText().toString());
                    cal.set(Integer.parseInt(startDate[2]), Integer.parseInt(startDate[1]) - 1, Integer.parseInt(startDate[0]),
                            Integer.parseInt(timer[0]), Integer.parseInt(timer[1]));
                    allMillis = allMillis.concat(String.valueOf(cal.getTimeInMillis()) + ",");
                }
                String last[] = breakTime(btn_sched[--i].getText().toString());
                String aMill[] = allMillis.split(",");
                cal.set(Integer.parseInt(endDate[2]), Integer.parseInt(endDate[1]) - 1, Integer.parseInt(endDate[0]),
                        Integer.parseInt(last[0]), Integer.parseInt(last[1]));
                startMilli = Long.parseLong(aMill[0]);
                endMilli = cal.getTimeInMillis();

                Log.d("allMillis", allMillis + " " + startMilli + " " + endMilli);
                long milli = 0;
                switch (spin_schedfreq.getSelectedItemPosition()) {
                    case 1:
                        milli = 24 * 60 * 60 * 1000;
                        break;
                    case 2:
                        milli = 2 * 24 * 60 * 60 * 1000;
                        break;
                    case 3:
                        milli = 3 * 24 * 60 * 60 * 1000;
                        break;
                    case 4:
                        milli = 4 * 24 * 60 * 60 * 1000;
                        break;
                    case 5:
                        milli = 5 * 24 * 60 * 60 * 1000;
                        break;
                    case 6:
                        milli = 6 * 24 * 60 * 60 * 1000;
                        break;
                    case 7:
                        milli = 7 * 24 * 60 * 60 * 1000;
                        break;
                    case 8:
                        milli = 14 * 24 * 60 * 60 * 1000;
                        break;
                    case 9:
                        milli = 21 * 24 * 60 * 60 * 1000;
                        break;
                    case 10:
                        milli = 28 * 24 * 60 * 60 * 1000;
                        break;
                    case 11:
                        milli = 30 * 24 * 60 * 60 * 1000;
                        break;
                    case 12:
                        milli = 60 * 24 * 60 * 60 * 1000;
                        break;
                }
                Log.d("allMillis", allMillis + " " + startMilli + " " + endMilli);

                m = new Medicine((et_medName.getText().toString()), ImageConvertor.getBytes(bitmap),
                        spin_type.getSelectedItemPosition(), et_dose.getText().toString(), et_inst.getText().toString(),
                        spin_schedType.getSelectedItemPosition(), btn_startD.getText().toString(),
                        btn_endD.getText().toString(), spin_schedfreq.getSelectedItemPosition(), milli, times, startMilli,
                        endMilli, allMillis);
            }
            long entry;
            if (getIntent().getIntExtra("UpdateMed", 0) == 1) {
                db.updateMedicine(m, getIntent().getIntExtra("Medicine DetailID", -1));
            } else
                entry = db.addMedicine(m);
            Intent launch_save = new Intent(AddMedActivity.this, HomeActivity.class);
            startActivity(launch_save);
            finish();
            return true;

        } else if (id == R.id.add_cancel) {
            Intent launch_cancel = new Intent(AddMedActivity.this, HomeActivity.class);
            startActivity(launch_cancel);
            finish();
            return true;
        } else if(id==android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    String[] breakDate(String str) {
        String date[] = str.split("-");
        for (int i = 0; i < 3; i++) {
            if (date[i].length() < 2)
                date[i] = "0" + date[i];
        }
        return date;
    }

    String[] breakTime(String str) {
        String time[] = str.split(":");
        for (int i = 0; i < 2; i++) {
            if (time[i].length() < 2)
                time[i] = "0" + time[i];
        }
        return time;
    }

    @Override
    public void onBackPressed() {
        Intent launch_main = new Intent(AddMedActivity.this, HomeActivity.class);
        startActivity(launch_main);
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_upImage:
                Intent launch_camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(launch_camera, 1995);
                break;
            case R.id.tv_expandDetails:
                if (rl_add.getVisibility() == View.GONE) {
                    rl_add.setVisibility(View.VISIBLE);
                    ll_schedule.setVisibility(View.GONE);
                } else {
                    rl_add.setVisibility(View.GONE);
                }setDrawable();
                break;
            case R.id.tv_expandSchedule:
                if (ll_schedule.getVisibility() == View.GONE) {
                    rl_add.setVisibility(View.GONE);
                    ll_schedule.setVisibility(View.VISIBLE);
                } else {
                    ll_schedule.setVisibility(View.GONE);
                }
                setDrawable();
                break;
            case R.id.btn_endD:
                displayDate(btn_endD);
                break;
            case R.id.btn_startD:
                displayDate(btn_startD);
                break;
            case R.id.btn_time1:
                displayTime(btn_sched[0]);
                break;
            case R.id.btn_time2:
                displayTime(btn_sched[1]);
                break;
            case R.id.btn_time3:
                displayTime(btn_sched[2]);
                break;
            case R.id.btn_time4:
                displayTime(btn_sched[3]);
                break;
            case R.id.btn_time5:
                displayTime(btn_sched[4]);
                break;
            case R.id.btn_time6:
                displayTime(btn_sched[5]);
                break;
            case R.id.btn_time7:
                displayTime(btn_sched[6]);
                break;
            case R.id.btn_time8:
                displayTime(btn_sched[7]);
                break;
            case R.id.btn_time9:
                displayTime(btn_sched[8]);
                break;
            case R.id.btn_time10:
                displayTime(btn_sched[9]);
                break;
            case R.id.btn_time11:
                displayTime(btn_sched[10]);
                break;
            case R.id.btn_time12:
                displayTime(btn_sched[11]);
                break;
            /*case R.id.btn_timeDays:
                displayTime(btn_timeDays);
        */}
    }

    void setDrawable() {
        if (rl_add.getVisibility() == View.GONE) {
            tv_expandDetails.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more, 0);
        } else {
            tv_expandDetails.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_less, 0);
        }
        if (ll_schedule.getVisibility() == View.GONE) {
            tv_expandSchedule.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more, 0);
        } else {
            tv_expandSchedule.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_less, 0);
        }

    }

    private int validate(View v) {
        if (et_medName.getText().toString() == "") {
            Snackbar.make(v, "Enter the medication name", Snackbar.LENGTH_LONG).show();
            return 1;
        }
        if (et_inst.getText().toString() == "") {
            Snackbar.make(v, "Enter the instructions for medication", Snackbar.LENGTH_LONG).show();
            return 1;
        }
        if (et_dose.getText().toString() == "") {
            Snackbar.make(v, "Enter the dosage of medication", Snackbar.LENGTH_LONG).show();
            return 1;
        }
        return 0;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1995 && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
//            iv_med.setImageBitmap(photo);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                fm_layout.setBackground(new BitmapDrawable(photo));
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (spin_schedType.getSelectedItemPosition() == 2) {
            ll_freq.setVisibility(View.VISIBLE);
            /*ll_days.setVisibility(View.GONE);
            */ll_date.setVisibility(View.VISIBLE);
            for (int j = 0; j < 12; j++) {
                if (j < spin_schedfreq.getSelectedItemPosition()) {
                    btn_sched[j].setVisibility(View.VISIBLE);
                    if ((getIntent().getIntExtra("UpdateMed", 0)) != 1)
                        timeSlots(j);
                } else {
                    btn_sched[j].setVisibility(View.GONE);
                    btn_sched[j].setText("__:__");
                }
            }
        } else if (spin_schedType.getSelectedItemPosition() == 1) {
            ll_freq.setVisibility(View.GONE);
/*
            ll_days.setVisibility(View.GONE);
*/
            ll_date.setVisibility(View.VISIBLE);
        } /*else if (spin_schedType.getSelectedItemPosition() == 3) {
            ll_freq.setVisibility(View.GONE);
            ll_date.setVisibility(View.VISIBLE);
            ll_days.setVisibility(View.VISIBLE);
        }*/ else if (spin_schedType.getSelectedItemPosition() == 0) {
            ll_freq.setVisibility(View.GONE);
/*
            ll_days.setVisibility(View.GONE);
*/
            ll_date.setVisibility(View.GONE);
        }
    }

    private void timeSlots(int j) {
        int h = cal.get(Calendar.HOUR_OF_DAY);
        int m = cal.get(Calendar.MINUTE);
        btn_sched[0].setText(h + ":" + m);

        for (int k = 1; k <= j; k++) {
            h = h + 24 / (j + 1);
            if (h >= 24)
                h = h - 24;
            m = m + ((24 / (j + 1)) % 10);
            if (m >= 60) {
                m = m - 60;
                h = h + 1;
            }
            btn_sched[k].setText(h + ":" + m);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        for (int j = 0; j < 12; j++)
            btn_sched[j].setVisibility(View.GONE);
    }
}