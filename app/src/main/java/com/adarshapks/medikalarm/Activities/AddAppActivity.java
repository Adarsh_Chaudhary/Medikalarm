package com.adarshapks.medikalarm.Activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.adarshapks.medikalarm.Helpers.DbHelper;
import com.adarshapks.medikalarm.Models.Appointment;
import com.adarshapks.medikalarm.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


/**
 * Created by Adarsh on 23-02-2017.
 */

public class AddAppActivity extends AppCompatActivity implements View.OnClickListener {
    EditText et_appName, et_appVenue, et_appSubject;
    TextView et_appTime, et_appDate;
    Button btn_appAlarm;
    private int mYear, mMonth, mDay, mHour, mMinute;
    final Calendar c = Calendar.getInstance();
    DbHelper db = new DbHelper(this);
    Appointment ap = new Appointment();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addapp);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Add");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left);

        btn_appAlarm = (Button) findViewById(R.id.btn_appAlarm);
        et_appDate = (TextView) findViewById(R.id.et_appDate);
        et_appName = (EditText) findViewById(R.id.et_appName);
        et_appTime = (TextView) findViewById(R.id.et_appTime);
        et_appVenue = (EditText) findViewById(R.id.et_appVenue);
        et_appSubject = (EditText) findViewById(R.id.et_appSubject);
        et_appDate.setClickable(true);
        et_appTime.setClickable(true);
        btn_appAlarm.setOnClickListener(this);
        et_appTime.setOnClickListener(this);
        et_appDate.setOnClickListener(this);
        if ((getIntent().getIntExtra("UpdateApp", 0)) == 1) {
            Log.d("ID RECIEVED", String.valueOf(getIntent().getIntExtra("Appointment DetailID", -1)));
            ap = db.getAppointment(getIntent().getIntExtra("Appointment DetailID", -1));

            et_appDate.setText(ap.getAppointDate());
            et_appTime.setText(ap.getAppointTime());
            et_appName.setText(ap.getAppointName());
            et_appSubject.setText(ap.getAppointSubject());
            et_appVenue.setText(ap.getAppointVenue());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add_save) {
            if (validate() == 0) {
                String date[] = et_appDate.getText().toString().split("-");
                for (int i = 0; i < 3; i++) {
                    if (date[i].length() < 2)
                        date[i] = "0" + date[i];
                }
                String time[] = et_appTime.getText().toString().split(":");
                for (int i = 0; i < 2; i++) {
                    if (time[i].length() < 2)
                        time[i] = "0" + time[i];
                }
                Calendar cal = Calendar.getInstance();
                cal.set(Integer.parseInt(date[2]), Integer.parseInt(date[1]) - 1, Integer.parseInt(date[0]), Integer.parseInt(time[0]), Integer.parseInt(time[1]));
                Log.d("date", (date[2]) + (date[1]) + (date[0]) + (time[0]) + (time[1]));
                Log.d("seconds", String.valueOf(cal.getTimeInMillis()));
                int alarmSet = 0;
                if (btn_appAlarm.getText().toString().equalsIgnoreCase("Alarm : true")) {
                    alarmSet = 1;
                }

                Appointment a = new Appointment((et_appName.getText().toString()), et_appSubject.getText().toString(), et_appVenue.getText().toString(),
                        et_appTime.getText().toString(), et_appDate.getText().toString(), cal.getTimeInMillis(), alarmSet);

                if (getIntent().getIntExtra("UpdateApp", 0) == 1) {
                    db.updateAppointment(a, getIntent().getIntExtra("Appointment DetailID", -1));
                } else {
                    long entry = db.addAppointment(a);
                    Log.d("EntryTable", String.valueOf(alarmSet));
                }
                Intent launch_main = new Intent(AddAppActivity.this, HomeActivity.class);
                launch_main.putExtra("Back to Home", 2);
                startActivity(launch_main);
                finish();

            }
            return true;
        } else if (id == R.id.add_cancel) {

            Intent launch_main = new Intent(AddAppActivity.this, HomeActivity.class);
            launch_main.putExtra("Back to Home", 2);
            startActivity(launch_main);
            finish();
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private int validate() {
        if (et_appName.getText().toString().equals("")) {
            Toast.makeText(this, "Enter Name of whom to meet", Toast.LENGTH_LONG).show();
            return 1;
        } else if (et_appVenue.getText().toString().equals("")) {
            Toast.makeText(AddAppActivity.this, "Enter Venue Details", Toast.LENGTH_LONG).show();
            return 1;
        } else if (et_appDate.getText().toString().equals("")) {
            Toast.makeText(this, "Enter Date of Appointment", Toast.LENGTH_LONG).show();
            return 1;
        } else if (et_appTime.getText().toString().equals("")) {
            Toast.makeText(this, "Enter Time of Appointment", Toast.LENGTH_LONG).show();
            return 1;
        } else if (et_appSubject.getText().toString().equals("")) {
            Toast.makeText(this, "Enter Subject of Appointment", Toast.LENGTH_LONG).show();
            return 1;
        }
        return 0;
    }

    @Override
    public void onBackPressed() {
        Intent launch_main = new Intent(AddAppActivity.this, HomeActivity.class);
        launch_main.putExtra("Back to Home", 2);
        startActivity(launch_main);
        finish();
        super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_appAlarm:
                if (btn_appAlarm.getText().toString().equalsIgnoreCase("Alarm : true"))
                    btn_appAlarm.setText("Alarm : false");
                else if (btn_appAlarm.getText().toString().equalsIgnoreCase("Alarm : false"))
                    btn_appAlarm.setText("Alarm : true");
                break;
            case R.id.et_appDate:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss", Locale.ENGLISH);
                        if ((year >= c.get(Calendar.YEAR)) && (monthOfYear >= c.get(Calendar.MONTH)) && (dayOfMonth >= c.get(Calendar.DAY_OF_MONTH))) {
                            et_appDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        } else
                            Toast.makeText(AddAppActivity.this, "Enter correct date", Toast.LENGTH_LONG).show();
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
                break;
            case R.id.et_appTime:
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                et_appTime.setText(hourOfDay + ":" + minute);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
                break;
        }
    }
}
