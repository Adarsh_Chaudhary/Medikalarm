package com.adarshapks.medikalarm.Activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.adarshapks.medikalarm.Helpers.DbHelper;
import com.adarshapks.medikalarm.Models.Appointment;
import com.adarshapks.medikalarm.Models.Ringer;
import com.adarshapks.medikalarm.R;
import com.adarshapks.medikalarm.Receivers.AppointReciever;

/**
 * Created by Adarsh on 24-02-2017.
 */

public class AlertAppointment extends AppCompatActivity {
    EditText et_nameAlertApp;
    Button btn_appStop;
    Ringtone notification;
    MediaPlayer mp = new MediaPlayer();
    DbHelper db;
    PendingIntent sender;
    AlarmManager alarmManager;
    Appointment ap = new Appointment();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alert_appointment);

        et_nameAlertApp = (EditText) findViewById(R.id.et_NameAlertApp);
        btn_appStop = (Button) findViewById(R.id.btn_appStop);
        db = new DbHelper(this);

        Ringer ring = db.getRinger();
        notification = RingtoneManager.getRingtone(getApplicationContext(), Uri.parse(ring.getRingApp()));
        mp = MediaPlayer.create(getApplicationContext(), Uri.parse(ring.getRingApp()));

        if ((getIntent().getIntExtra("RecievedAppoint", 0)) != 0) {
            Log.d("ID RECIEVED", String.valueOf(getIntent().getIntExtra("Appointment DetailID", -1)));
            ap = db.getAppointment(getIntent().getIntExtra("Appointment DetailID", -1));
        }
        Log.d("write", ap.getAppointName());

        if ((getIntent().getIntExtra("RecievedAppoint", 0) == 1) && ap.getAppointAlarm() == 1) {
            Intent intent = new Intent(this, AppointReciever.class);
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            sender = PendingIntent.getBroadcast(this, 0, intent, 0);
            alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            if (ring.getVibe() == 1) {
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(1000);
            }
            db.updateAlarmAppointment(db.getAppointment(getIntent().getIntExtra("Appointment DetailID", -1)), 0);
            Appointment next = db.nextAlarmApp();
            if (next != null) {
                Log.d("next", next.getAppointName());
                intent.putExtra("Appointment ID", next.getAppointId());
                sender = PendingIntent.getBroadcast(this, next.getAppointId(), intent, 0);
                alarmManager.set(AlarmManager.RTC_WAKEUP, next.getAppointMillis(), sender);
            }
            mp.start();
            mp.setLooping(true);
        }

        et_nameAlertApp.setClickable(false);
        et_nameAlertApp.setFocusable(false);

        if (getIntent().getIntExtra("RecievedAppoint", 0) == 2) {
            et_nameAlertApp.setText("You have an Appointment with " + ap.getAppointName() + " for " + ap.getAppointSubject()
                    + " at " + ap.getAppointTime() + " on " + ap.getAppointDate() + ". Planned at " + ap.getAppointVenue());
            btn_appStop.setVisibility(View.GONE);
        } else {
            et_nameAlertApp.setText("You have an Appointment with " + ap.getAppointName() + " for " + ap.getAppointSubject()
                    + " at " + ap.getAppointTime() + " today. Lets get to " + ap.getAppointVenue()
                    + " for it");
            btn_appStop.setVisibility(View.VISIBLE);
        }
        btn_appStop.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                mp.stop();
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        mp.stop();
        finish();
    }

}
